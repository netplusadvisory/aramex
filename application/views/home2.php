<!DOCTYPE html>
<html>
<head>

	<title>Shop & Ship</title>

	<meta name="viewport" content="width=device-width" />
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/new_style.css">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
</head>
<body class="signup">
	<!-- header -->
	
	<!-- end header -->
	<?php
		// on page load, destroy session if it exists
		session_destroy();
		foreach ($_SESSION as $key => $value) {
		    unset($_SESSION[$key]);
		}
	?>
<div id="loader"><p>Loading...</p></div>  
<div id="wrapper">
	
	<div id="signup">

	        <!-- form step1 -->
	        <div id="step1">					
				<?php 
					// Form Open
					echo form_open(base_url()."index.php/Site/user_data_submit"); 
				?>

				<div>
					<p class="wrong_place">Having troubles? <a href="#">Get help</a></p>
        			<h2>Aramex</h2>
				</div>

				<div class="btn-group btn-breadcrumb">
		            <a href="JavaScript:void(0);" class="btn btn-bg-blue active" id="step1_breadcrumbs">STEP 1</a>
		            <a href="JavaScript:void(0);" class="btn btn-bg-blue" id="step2_breadcrumbs">STEP 2</a>
		            <a href="JavaScript:void(0);" class="btn btn-bg-blue" id="step3_breadcrumbs">STEP 3</a>
		        </div>
		        <p class="text light-red" >Please fill all fields to proceed!!!</p>
				<p class="text error_msg red"></p>


				<div id="voucher_amount">
					<p class="text">
					<label>Voucher Amount ($)</label>
					<?php 
						// voucher amount
						$data_name = array(
							'type' => 'number',
							'class' => 'form-control bg-sky-blue no-border-radius',
							'placeholder' => 'Enter Voucher amount in dollars',
							'id' => 'voucherAmount',
							'name' => 'voucherAmount',
							'min' => '0',
							'step' => '0.1'
						);
						echo form_input($data_name);							
					?>
					</p>
					<p class="text validate convertToNaira green" style="display: none;">Voucher Amount: &#8358;<span></span></p>
					<p class="text validate cardFee" style="display: none;">Card Issuance Fee: &#8358;<span></span>
					<p class="text validate charge" style="display: none;">Service charge: &#8358;<span></span></p> 
					<p class="text validate total_amount green" style="display: none;">Total Amount: &#8358;<span></span></p>
				<!--	<p class="text validate convertToNaira white" style="display: none;">Your BVN details will be required</p>
				-->
					<input type="hidden" name="rate" id="dollarRate" value="">
					<input type="hidden" name="service_charge" id="serviceCharge" value="">
					<input type="hidden" name="card_fee" id="cardFee" value="">
					
					
				</div>
					

					
				<p class="text light-red" id="bvn_text">Your BVN details will be required</p>

				<div id="user_bvn_verification" style="display: none;">
					<p class="text">
						<label>Enter BVN</label>
						<?php 
							// bvn
							$data_name = array(
								'type' => 'number',
								'name' => 'user[email]',
								'placeholder' => 'Provide BVN',
								'id' => 'bvn'
							);
							echo form_input($data_name);							
						?>
					</p>
					<p class="text validate bvn_status red" style="display: none;"></p>	
				</div>


				<div id="user_data" style="display: none;">

				<p class="text">
					<label>Firstname</label>
					<?php 
						// firstname
						// $class = ['form-control', 'bg-sky-blue', 'no-border-radius'];
						$data_name = array(
							'type' => 'text',
							'class' => 'form-control bg-sky-blue no-border-radius',
							'placeholder' => 'Enter firstname',
							'id' => 'firstname',
							'name' => 'firstname'
						);
						echo form_input($data_name);							
					?>
					<span class="text light-red" id="fname" style="display: none">Your firstname is required*</span>
				</p>

				<p class="text">
					<label>Lastname</label>
					<?php 
						// lastname
						$data_name = array(
							'type' => 'text',
							'class' => 'form-control bg-sky-blue no-border-radius',
							'placeholder' => 'Enter lastname',
							'id' => 'lastname',
							'name' => 'lastname'
						);
						echo form_input($data_name);							
					?>
					<span class="text light-red" id="lname" style="display: none">Your lastname is required*</span>
				</p>


				<p class="text">
					<label>Email</label>
					<?php 
						// email
						$data_name = array(
							'type' => 'email',
							'class' => 'form-control bg-sky-blue no-border-radius',
							'placeholder' => 'Enter email',
							'id' => 'email',
							'name' => 'email',
							'required' => 'required'
						);
						echo form_input($data_name);							
					?>
					<span class="text light-red" id="mail" style="display: none">Your email is required*</span>
				</p>
				<p class="text validate email_status red" style="display: none;"></p>

				</div>	

					<div id="proceed_button">
						<p class="submit">
							<?php 
								// submit button
								$data_name = array(
						        'type' => 'submit',
						        'class' => 'btn theme-btn',
						        'id' => 'step1_button',
						        'content' => '<span>PROCEED</span>'
								);
								echo form_button($data_name);							
							?>
						</p>

						
					</div>				

					<?php
						// Form Close
						echo form_close(); 
					?>
  
	        </div>
	        <!-- end form step1-->

	        
	        <div id="step2" style="display: none;">
				<div id="payment_data">
					
					<?php 
						// Form Open
						echo form_open("https://netpluspay.com/testpayment/paysrc/"); 
					?>
					<div>
						<p class="wrong_place">Having troubles? <a href="#">Get help</a></p>
	        			<h2>Aramex</h2>
					</div>


					<div class="btn-group btn-breadcrumb">
			            <a href="JavaScript:void(0);" class="btn btn-bg-blue " id="step1_breadcrumbs">STEP 1</a>
			            <a href="JavaScript:void(0);" class="btn btn-bg-blue  active" id="step2_breadcrumbs">STEP 2</a>
			            <a href="JavaScript:void(0);" class="btn btn-bg-blue" id="step3_breadcrumbs">STEP 3</a>
			        </div>
			       


					<p class="text light-red">Please click on pay to continue!!!</p>
					<p class="text error_msg red"></p>

					
					<div class="infobox">
						<h1>Make sure you have your bvn details before you pay for the voucher.</h1><hr><h3>You can now check your bank verification number on your mobile phones. simply Dial *565*0# to check your Bank Verification Number "BVN" from your Mobile phones at N10 only.</h3>
					</div>
					<br><br>

					<p class="submit" id="pay_">
						<?php 
							// submit button
							$data_name = array(
					        'type' => 'submit',
					        'class' => 'btn theme-btn',
					        'id' => 'makePayment',
					        'content' => '<span>PAY</span>'
							);
							echo form_button($data_name);							
						?>
					</p>

					<p class="submit" style="display: none;">
						<?php 
							// submit button
							$data_name = array(
					        'type' => 'submit',
					        'class' => 'btn theme-btn',
					        'id' => 'restart',
					        'content' => 'START AGAIN'
							);
							echo form_button($data_name);							
						?>
					</p>

					<input type="hidden" name="merchant_id" value="TEST57761f53d9ca8">
                    <input type="hidden" name="currency_code" id="currency_code" value="NGN">
                    <input type="hidden" name="narration" value="Aramex Website Demo test">
                    <input type="hidden" name="order_id" value="59536cb40e482">
                    <input type="hidden" name="full_name" value="abu odi">
                    <input type="hidden" name="return_url" value="<?php echo base_url(); ?>index.php/Site/result">
                    <input type="hidden" name="recurring" value="no">
                    <input type="hidden" name="total_amount" value="200">


					<?php
						// Form Close
						echo form_close(); 
					?>

		        </div>

		        <!-- Display Result Using Ajax -->
		        <!-- form -->
		        <div class="row row-centered pb-20" id='payment_result_block' style='display: none'>
					<div class="col-md-8 col-centered">
						<form class="form">
						  <div id="payment_result"></div>
		
						  <div class="text-right" id="payment_result_button">
						  	
						  </div>
						</form>
			        </div>
		        </div>
		        <!-- end form -->
		        
	        </div>
	        <!-- end form step3-->

	  	<!-- </div> -->
	  <!-- </div> -->

	</div>
</div>
<!-- end page body -->



<!-- javascript -->
<script type="text/javascript">
	// Ajax post
	// show the loading image before calling ajax.

	$('#proceed_button').hide();
	$(document).ready(function() {

		// hide loading image
		// hideLoadingImage();
		// error section
		$("p.error_msg").html("");
		// $('.convertToNaira').hide();
		$('.convertToNaira span').html('');
		var step1Data = {};
		
		var email = $('input#email').val();

			// Get the service charges from the database
			$.getJSON("<?php echo base_url(); ?>" + "index.php/service_charge/?active=true").done(function(data){
				var service_charge = data.charges;
				$(".charge").val(service_charge);
				$("input#serviceCharge").val(service_charge);

				// Get the card fee charges from the database
				$.getJSON("<?php echo base_url(); ?>" + "index.php/card_fee/?active=true").done(function(data){
					var card_fee = data.fee;
					$(".cardFee").val(card_fee);
					$("input#cardFee").val(card_fee);

					// Get the dollar conversion rate
					$.getJSON("<?php echo base_url(); ?>" + "index.php/rate?active=true").done(function(data) {
						// check if ajax works
						// alert('the get request works');

						var rate = data.rate;
						 $("input#dollarRate").val(rate);
						// show equivalent of dollars in naira
						$("input#voucherAmount").bind("change paste keyup", function(event) {
							event.preventDefault();
							var voucherAmount = $("input#voucherAmount").val();
							voucherAmount = voucherAmount.trim();


							var element = ".convertToNaira";
							if (voucherAmount != "") {
								naira_value = convertToNaira(voucherAmount, rate, element);
								var total_amount = parseInt(naira_value) + parseInt(service_charge) + parseInt(card_fee);
								$('#user_data').show();
								$('#proceed_button').show();
								$('.charge').show();
								$('.charge span').show().html(service_charge);
								$('.cardFee').show();
								$('.cardFee span').show().html(card_fee);
								$('.total_amount').show();
								$('.total_amount span').html(total_amount);
								$('#bvn_text').hide();

							}else{
								$('#user_data').hide();
								$('.convertToNaira').hide();
								$('.cardFee').hide();
								$('.charge').hide();
								$('.total_amount').hide();
								$('#proceed_button').hide();
								$('#user_bvn_verification').hide();
							}
							
						});	
						
					})

				})

			})
			// if rate is not returned or ajax didnt work
			.fail(function(jqXHR, textStatus, errorThrown) {
				$("p.error_msg").html("Rate was not returned <br> Please contact the administrator ");
			});

		
		// validate BVN
		$('input#bvn').bind("paste keyup", function(event) {
			event.preventDefault();
			var bvn = $("input#bvn").val();
			if (bvn != "") {
				$('#bvn_text').hide();
				if(validateBVN()){
					$('.bvn_status').show();
					$('.bvn_status').html('Invalid BVN');
					$('#user_data').hide();
				}else{
					$('.bvn_status').hide();
					// if bvn is wrong do nothing else show other user information
					// get user data from db
					// show the loading image before calling ajax.
					$("#loader").show();
					$.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>" + "index.php/Site/get_bvn_data",
						dataType: 'json',
						data: { bvn: bvn },
						success: function(res) {
							if (res){
								// show user bvn data in form
								// alert("firstname: " + res.firstname + " lastname: " + res.lastname + " email: " + res.email);
								$('#firstname').val(res.firstname);
								$('#lastname').val(res.lastname);
								$('#email').val(res.email);
								// show form
								$('.bvn_status').hide();
								$('#user_data').show();
							}						
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
						    // alert("validation from db didnt work");
						    $('.bvn_status').show();
						    $('.bvn_status').html('BVN does not exist');
						    // hide loading image
							$("#loader").hide();
						},
						complete: function() {
							// hide loading image
							$("#loader").hide();
						}
					});
					
					
				}
			}else{
				$('.bvn_status').hide();
			}
		});

		//validate firstname
		$('input#firstname').bind("paste keyup change", function(event){
			event.preventDefault();
			var firstname = $("input#firstname").val();
			if(firstname != ""){
				$('#fname').hide();

			}else{
				$('#fname').show();
				$('#fname').css("display", "inline");
			}
		});

		//validate lastname
		$('input#lastname').bind("paste keyup change", function(event){
			event.preventDefault();
			var lastname = $('input#lastname').val();
			if(lastname != ""){
				$('#lname').hide();
			}else{
				$('#lname').show();
				$('lname').css("dispaly", "inline");
			}
		});


		// on step 1 button click submit form
		$("#step1_button").click(function(event) {
			event.preventDefault();

			//Checks if user inputed firstname
			if(firstname != ""){
				$('#fname').hide();
			}else{
				$('#fname').show();
				$('#fname').css("display", "inline");
			}

			//Checks if user inputed lastname
			if(lastname  != ""){
				$('#lname').hide();
			}else{
				$('#lname').show();
				$('#lname').css("display", "inline");
			}


			//validate firstname
			$('input#firstname').bind("paste keyup change", function(event){
				event.preventDefault();
				var firstname = $("input#firstname").val();
				if(firstname != ""){
					$('#fname').hide();

				}else{
					$('#fname').show();
					$('#fname').css("display", "inline");
				}
			});

			//validate lastname
			$('input#lastname').bind("paste keyup change", function(event){
				event.preventDefault();
				var lastname = $('input#lastname').val();
				if(lastname != ""){
					$('#lname').hide();
				}else{
					$('#lname').show();
					$('#lname').css("dispaly", "inline");
				}
			});

			// validate email
			$('input#email').bind("paste keyup change", function(event) {
				event.preventDefault();
				var email = $("input#email").val();
				if (email != "") {
					if(!validateEmail(email)){
						$('.email_status').show();
						$('.email_status').html('Invalid email');
					}else{
						$('.email_status').hide();

					}
				}else{
					$('.email_status').show();
					$('.email_status').html('Email is required*');

				}
			});


			step1Data.firstname = $("input#firstname").val();
			step1Data.lastname = $("input#lastname").val();
			step1Data.dollarAmount = $("input#voucherAmount").val();
			step1Data.voucherAmount = $('.total_amount span').html();
			step1Data.email = $("input#email").val();
			//step1Data.amount_naira = $('input#convertToNaira').val();

			// check if input is valid else gives error...check for this...|| checkIfInputHasMoreThanOneDot(voucherAmount) 
			if (checkIfEmpty(step1Data.voucherAmount) || checkIfEmpty(step1Data.firstname) || checkIfEmpty(step1Data.lastname) || checkIfEmpty(step1Data.email)) {
				$("p.error_msg").html("All fields are requried, Please enter valid details");
			}else if(checkIfEmpty(step1Data.voucherAmount) ){
				$("p.error_msg").html("Voucher amount is invalid");
			}else{
				$("p.error_msg").html("");
				// show the loading image before calling ajax.
				
				$("#step1").hide();
				$("#loader").show();
                $.ajax({
                	url : "<?php echo base_url(); ?>" + "index.php/Site/user_data_submit",
					dataType: 'json',
                	type : "post",
                	data : step1Data,
                	success : function(res) {
                		$("#step2").show();
                		$("#step1_breadcrumbs").removeClass('active');
						$("#step2_breadcrumbs").addClass('active');
						$("#step3_breadcrumbs").removeClass('active');	
                	},
                	error: function(XMLHttpRequest, textStatus, errorThrown) {
			    		alert("form did not submit error");
					},
					complete: function() {
						// hide loading image
						$("#loader").hide();
					}
                });

        		$.getJSON("<?php echo base_url(); ?>" + "index.php/Site/get_user_data").done(function(data) {
					var transaction_id = data.transaction_id;
					var total = $('.total_amount span').html();
					var fullname = data.firstname + ' ' + data.lastname;
					$("input[name='order_id']").val(transaction_id);
					$("input[name='total_amount']").val(total);
					$("input[name='full_name']").val(fullname);
				})


				
			}

		});

	

		function getUserData(){
			$.getJSON("<?php echo base_url(); ?>" + "index.php/Site/get_user_data").done(function(data) {
				// check if ajax works
				// alert('the get request works');
				// get user data
				return data;
		        // check if it has values
		        // alert('payment_id: '+payment_id);
					
			})
		}


		function validateBVN(){
			var bvn = $("input#bvn").val();
			if((bvn.toString().length) != 10){
				// error
				return true;
			}else{
				return false;
			}
		}


		// Function that validates email address through a regular expression.
		function validateEmail(Email_input) {
			var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
			if (filter.test(Email_input)) {
				return true;
			}
			else {
				return false;
			}
		}
		
		// function that checks if its empty
		function checkIfEmpty(value){
			// var id = '#'+id;
			// id = '"'+'"';
			// var value = $(id).val();
			if ($.trim(value).length == 0 || value == "") {
				// alert('All fields are mandatory');
				return true;
			}
			else {
				return false;
			}
		}

		// convert to naira
		function convertToNaira(number, rate, element){
			var nara_value = number * rate;
			nara_value = Number(nara_value).toFixed(2); 
			// return $nara_value;
			$(element).show();
			element_span = element + " span";
			$(element_span).html(nara_value);

			return nara_value;
		}


	});
</script>

<script type="text/javascript">
	// Select your input element.
	var voucherAmount = document.getElementById('voucherAmount');
	var bvn = document.getElementById('bvn');
	// var accountNumber = document.getElementById('accountNumber');

	// Listen for input event on numInput. Allows decimal point 110 & 190
	voucherAmount.onkeydown = function(e) {
	    if(!((e.keyCode > 95 && e.keyCode < 106)
	      || (e.keyCode > 47 && e.keyCode < 58) 
	      || e.keyCode == 190
	      || e.keyCode == 110
	      || e.keyCode == 9
	      || e.keyCode == 46
	      || e.keyCode == 8)) {
	        return false;
	    }
	}

	
	bvn.onkeydown = function(e) {
	    if(!((e.keyCode > 95 && e.keyCode < 106)
	      || (e.keyCode > 47 && e.keyCode < 58)
	      || e.keyCode == 9
	      || e.keyCode == 46
	      || e.keyCode == 8)) {
	        return false;
	    }
	}

	// accountNumber.onkeydown = function(e) {
	//     if(!((e.keyCode > 95 && e.keyCode < 106)
	//       || (e.keyCode > 47 && e.keyCode < 58)
	//       || e.keyCode == 9
	//       || e.keyCode == 46
	//       || e.keyCode == 8)) {
	//         return false;
	//     }
	// }
	// ensures you have only one decimal point
	// $('#sub').Click(function(event){
	// 	var number = $('input#number').val();
	function checkIfInputHasMoreThanOneDot(number){
		if ((number.match(/./gi)) == "") {
			return true;
		}
		else if((number.match(/./gi).length) == 1){
			return true;
		}
		else{
			return false;
		}
	}
	// });
</script>

</body>
</html>