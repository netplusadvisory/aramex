<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Aramex</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="/assets/css/landing-page.css" rel="stylesheet">
	

<script type = "text/javascript">
    function showForm(){
        var selopt = document.getElementById("opts").value;
        if (selopt == 1) {
          document.getElementById("f1").style.display="block";
          document.getElementById("f2").style.display="none";
        }
        if (selopt == 2) {
            document.getElementById("f2").style.display="block";
            document.getElementById("f1").style.display="none";
        }
    }
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>


  </head>

  <body>
    <!-- Navigation -->
	
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
	    <div class="container">      
            <div class="col-lg-2">
                <a class="navbar-brand" href="#"><img src="/assets/images/aramex_logo_main.jpg"></a>
            </div><!--end of col-lg-4-->
	    	 <div class="col-lg-10">
		        <div class="for-support">
                    For Support call : 0801 2345 678
		        </div>
	        </div>   
		</div>
    </nav>
  
    <!-- Page Content -->
	  <section>
	    
		  <div class="container" style="margin-top:150px;">
	        <div class="row">
		
		      <div class="col-lg-12">
			      <div class="container-main" style="height:100%;">
				     <div class="welcome-note">
				        <h2>Welcome to Aramex <br/>
						   <span style="border-bottom:4px solid #CB1A0D;">Portal</span></h2>
						<p> 
						  <h4>IMPORTANT!</h4> 
						</p>
						
						<p>
						   Make sure you have your BVN details before you pay for the voucher. You can now check your bank verification number on your mobile phone.  <br/><br/>

                           Simply dial *565*0# to check your Bank Verification Number (BVN) on your mobile phone at N10 only.
						</p>
					</div><!--end of welcome-note-->
					
					<a class="submit-btn" href="/start" style="margin-right:0px">PROCEED</a>
					<!--end of proceed-btn-->
					
				</div><!--end of container-main-->
					
					<div class="footer-main">
					     <img src="/assets/images/aramex_logo_main2.jpg" class="img-reponsive"  />
					</div><!--end of footer-main-->
		      </div>
		  
		   </div>
		 </div> 
	 </section>

    <!-- Bootstrap core JavaScript -->
    <!-- <script src="/assets/bootstrap/js/bootstrap.min.js"></script> -->
  </body>
</html>