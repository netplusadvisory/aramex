<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Aramex</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="/assets/nouislider/nouislider.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="/assets/css/landing-page.css" rel="stylesheet">
	

<script type = "text/javascript">
    function showForm(){
        var selopt = document.getElementById("opts").value;
        if (selopt == 1) {
            document.getElementById("f1").style.display="block";
            document.getElementById("f2").style.display="none";
        }
        if (selopt == 2) {
            document.getElementById("f2").style.display="block";
            document.getElementById("f1").style.display="none";
        }
    }
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        // Add smooth scrolling to all links
         $("a").on('click', function(event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                event.preventDefault();
                var hash = this.hash;
                $('html, body').animate({
                scrollTop: $(hash).offset().top
                }, 800, function(){
   
                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
                });
            }
        });
    });
</script>

</head>

  <body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
	    <div class="container">          
        <div class="col-lg-2">
            <a class="navbar-brand" href="#"><img src="/assets/images/aramex_logo_main.jpg"></a>
        </div><!--end of col-lg-4-->
	     
	  	<div class="col-lg-10">
	        <div class="for-support">
                For Support call : 0801 2345 678
	        </div>
	    </div>
		   </div>
    </nav>
  
    <!-- Page Content -->
	<section>
		<div class="container" style="margin-top:150px;">
            <div class="container-main" style="height:100%; padding:0;">
	            <div class="row">
				    <div class="col-lg-4 step1-header active-header">
				        <h3>STEP 1</h3>
					</div>
					<div class="col-lg-4 step2-header">
					    <h3>STEP 2</h3>
                    </div>
					<div class="col-lg-4 step2-header">
					    <h3>STEP 3</h3>
                    </div>                    
                </div>
                <div class="row">
                    <div class="col-lg-12">         
				        <div class="form-container">
                            <form id="step1Form" method="post" action="/payment">
                                <div class="form-box">
                                    <p class="section-title">Tell us a bit about you</p>
                                </div>                                
                                <div class="form-box" style="margin-top: 15px">
                                    <input type="text" class="form-control" name="fullname" id="fullname" placeholder="Fullname" required>
                                </div>
                                <div class="form-box">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email address" required>
                                </div>                                
                                <div class="form-box" style="margin-top:40px">
                                    <p class="section-title">How much dollars do you need?</p>
                                </div>
                                <div class="form-box" style="margin-top:0px"> 
                                    <p class="dollar-amount">$<span id="inside_dollar_amount">0</span></p>
                                </div>
                                <div class="form-box" style="margin-top:15px;">
                                    <div id="dollar" class="noUi-target noUi-ltr noUi-horizontal"></div>
                                    <input type="text" class="immutable-form-control" name="dollar_value" id="dollar_value" required>
                                </div>
					            <div class="immutable-form-box" style="margin-top: 15px">
                                    <div class="label">Naira Equivalent</div>
                                    <div class="value"><span id="inside_naira_equivalent">0</span> <i style="font-weight: 500;font-size: 12px">(ngn)</i></div>
					            </div>
					            <div class="immutable-form-box">
                                    <div class="label">Card Issuance Fee</div>
                                    <div class="value"><span id="inside_card_issuance_fee">0</span> <i style="font-weight: 500;font-size: 12px">(ngn)</i></div>
                                </div>
					            <div class="immutable-form-box">
                                    <div class="label">Service Charge</div>
                                    <div class="value"><span id="inside_service_charge">0</span> <i style="font-weight: 500;font-size: 12px">(ngn)</i></div>
                                </div>
                                <div class="immutable-form-box">
                                    <div class="label">Total Amount</div>
                                    <div class="value"  style="background:#416F00; color: #fff"><span id="inside_total_amount">0</span> <i style="font-weight: 500;font-size: 12px">(ngn)</i></div>
                                </div>
                                <a href="" style="width:150px" class="submit-btn">PAY</a>
                            </form>
                        </div> 
                    </div> 
                </div>
                <div class="footer-main" style="width:100%">
		            <img src="/assets/images/aramex_logo_main2.jpg" class="img-reponsive"  />
		        </div>
            </div>
		</div>
	</section>
		     
    <!-- JavaScript -->
    <script src="/assets/nouislider/nouislider.min.js"></script>
    <script src="/assets/bootstrap/js/popper.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="/assets/js/step1.js"></script>

  </body>

</html>