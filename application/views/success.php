<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Aramex</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="/assets/css/landing-page.css" rel="stylesheet">
    <script type = "text/javascript">
        function showForm(){
            var selopt = document.getElementById("opts").value;
            if (selopt == 1) {
                document.getElementById("f1").style.display="block";
                document.getElementById("f2").style.display="none";
            }
            if (selopt == 2) {
                document.getElementById("f2").style.display="block";
                document.getElementById("f1").style.display="none";
            }
        }
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
        // Add smooth scrolling to all links
        $("a").on('click', function(event) {
        // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
            // Prevent default anchor click behavior
                event.preventDefault();
                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function(){
                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });
    });
    </script>
  </head>

  <body>

    <!-- Navigation -->
	
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
	    <div class="container">
            <div class="col-lg-2">
                <a class="navbar-brand" href="#"><img src="/assets/images/aramex_logo_main.jpg"></a>
            </div><!--end of col-lg-4-->
	    	<div class="col-lg-10">
		        <div class="for-support">
                    For Support call : 0801 2345 678
		        </div>
	        </div>
	    </div>
    </nav>

    <!-- Page Content -->	
	<section>
		<div class="container" style="margin-top:150px;">
	        <div class="row">
		      <div class="col-lg-12">
			        <div class="container-main" style="height:100%; padding:0;">
					    <div class="success-note">
					        <h1>SUCCESS</h1>
						    <div style="border-bottom:4px solid #cb1a0d; width:25%;"></div>
						    <p>You have successfully created your Aramex virtual card, details will be sent to your email address. Your card will be activated within 24hours</p>
					    </div>					   
					    <div class="card-informatiom">
					        <div class="card-container">    
							    <div class="card-header" align="right">
							        <img src="/assets/images/netplus_logo.jpg"/>
							    </div><!--end of card-header-->
							    <div class="card-body">
							        <div class="aramex-logo">
							            <img src="/assets/images/aramex_logo.fw.png" />
							        </div><!--end of aramex-logo-->
							        <div class="card-no">
							            4532        <span>xxxx</span>       <span>xxxx</span>       <span>xxx5</span>
							        </div><!--end of card-no-->
							        <div class="card-validity">
							            VALID THRU: 9/20
							        </div><!--end of card-validity-->
							        <div class="mastercard-logo" align="right">
							            <img src="/assets/images/mastercard_logo.fw.png" />
							        </div><!--end of mastercard-logo-->
							        <div class="card-name" align="left">
							            <?php echo $card_name ?>
							        </div>
                                </div><!--end of card-body-->
                            </div><!--end of card-container-->
					    </div><!--end of card-information-->
					        <a class="submit-btn" href="/">BACK TO HOME</a>
				    </div><!--end of container-main-->
					<div class="footer-main">
					     <img src="/assets/images/aramex_logo_main2.jpg" class="img-reponsive"  />
					</div><!--end of footer-main-->
		      </div>
		   </div>
		 </div> 
	 </section>
		     		
    <!-- Bootstrap core JavaScript -->
    <script src="/assets/bootstrap/js/popper.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>

  </body>

</html>