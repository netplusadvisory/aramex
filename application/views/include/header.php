<nav class="navbar navbar-light bg-faded">
		<div class="container-fluid">
    		<div class="navbar-header">
    			<a class="navbar-brand" href="#"><img src="assets/images/ship-n-shop-logo.png"></a>
    		</div>
    		<ul class="nav navbar-nav">
      		<!-- <li class="active"><a href="#">Home</a></li> -->
	      		<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">ABOUT <span class="caret"></span></a>
	        		<ul class="dropdown-menu">
	        			<li><a href="#">Menu 1</a></li>
	        			<li><a href="#">Menu 2</a></li>
	        			<li><a href="#">Menu 3</a></li>
	        		</ul>
	      		</li>
	      		<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">FEATURES <span class="caret"></span></a>
	        		<ul class="dropdown-menu">
	        			<li><a href="#">Menu 1</a></li>
	        			<li><a href="#">Menu 2</a></li>
	        			<li><a href="#">Menu 3</a></li>
	        		</ul>
	      		</li>
	      		<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">HELP <span class="caret"></span></a>
	        		<ul class="dropdown-menu">
	        			<li><a href="#">Menu 1</a></li>
	        			<li><a href="#">Menu 2</a></li>
	        			<li><a href="#">Menu 3</a></li>
	        		</ul>
	      		</li>
	      		<li><a href="#">VOUCHER</a></li>
    		</ul>
    		<form class="navbar-form navbar-left">
      			<div class="form-group">
       			 <input type="text" class="form-control" placeholder="Track your shipment" style="border-radius: 0; -webkit-box-shadow: none; box-shadow: none;">
      			</div>
      			<button type="submit" class="btn btn-default" style="background: #2B245B; border-radius: 0; margin-left: -4px; border: 1px solid #2b245b;"><span class="glyphicon glyphicon-search" style="color: white;"></span></button>
    		</form>
		    <ul class="nav navbar-nav navbar-right">
		      <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
		      <li style="background: #2B245B;"><a href="#" style="color: white;"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
		      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">English <span class="caret"></span></a>
	        		<ul class="dropdown-menu">
	        			<li><a href="#">Menu 1</a></li>
	        			<li><a href="#">Menu 2</a></li>
	        			<li><a href="#">Menu 3</a></li>
	        		</ul>
	      		</li>
		    </ul>
  		</div>
	</nav>