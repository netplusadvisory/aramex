<!-- footer -->
<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-6 about-us">
				<h1>About Us</h1>
				<div class="description">
					<p>What is Shop & Ship</p>
					<p>How to Shop & Ship</p>
					<p>Terms & Conditions</p>
					<p>Service Update</p>
					<p>Service Fees</p>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 about-us">
				<h1>Features</h1>
				<div class="description">
					<p>Shopping Directory</p>
					<p>S&S Protect</p>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 about-us">
				<h1>Legal</h1>
				<div class="description">
					<p>Privacy Policy</p>
					<p>Terms of Use</p>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 about-us">
				<h1>Contact Us</h1>
				<div class="social-icons">
					<ul>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- end footer -->
<!-- copyright -->
<div class="bg-blue copyright">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">Shop & Ship is an online shipping delivery service from Aramex | Copyright © 2017 Aramex International LCC | All rights reserved.</div>
		</div>
	</div>
</div>
<!-- end copyright -->
