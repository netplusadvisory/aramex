<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Aramex</title>


    <!-- Bootstrap core CSS -->
    <link href="/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="/assets/css/landing-page.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    
    <script>
        $(function() {
            $("#dialog").modal("show");
        });
    </script>
  </head>

  <body>
    <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
	        <div class="container"> 
                <div class="col-lg-2">
                    <a class="navbar-brand" href="#"><img src="/assets/images/aramex_logo_main.jpg"></a>
                </div><!--end of col-lg-4-->
	    	    <div class="col-lg-10">
		            <div class="for-support">
                        For Support call : 0801 2345 678
		            </div>
	            </div>
		    </div>
        </nav>
        <div class="modal fade" data-backdrop="static" data-keyboard="false" id="dialog" tabindex="-1" role="dialog" aria-labelledby="errorDialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div style="text-align:center;margin-top:20px;margin-bottom:30px">
                                <h4>BVN VERIFICATION FAILED</h4>
                            </div>
                            <div style="text-align:center">
                                <p>We recognize that you've been charged. Please, for security reasons, don't attempt a retry. For further enquiry call 08012345789 or send an email to info@aramexcard.com.</p>
                            </div>
                            <div style="">
                                <a style="width:auto;margin-left:auto;margin-right:0px;margin-bottom:10px;margin-top:10px;" href="/" class="submit-btn">BACK TO HOME</a>
                            </div>
                        </div>
                    </div>
                    <div style="height:25px;width:100%;background:lightgray;"></div>
                </div>
            </div>
        </div>

        <!-- JavaScript -->
        <script src="/assets/bootstrap/js/popper.min.js"></script>
        <script src="/assets/bootstrap/js/bootstrap.min.js"></script>

  </body>

</html>