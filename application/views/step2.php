<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Aramex</title>

    <!-- Bootstrap core CSS -->
    <link href="/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="/assets/nouislider/nouislider.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="/assets/css/landing-page.css" rel="stylesheet">
	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>
        $(function(){
            $("#submit-btn").on('click',function(e) {
                e.preventDefault();
                $('#form').submit();
            });
        });
    </script>

</head>

 <body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
	    <div class="container">          
            <div class="col-lg-2">
                <a class="navbar-brand" href="#"><img src="/assets/images/aramex_logo_main.jpg"></a>
            </div><!--end of col-lg-4-->	     
	  	    <div class="col-lg-10">
	            <div class="for-support">
                    For Support call : 0801 2345 678
	            </div>
	        </div>
		</div>
    </nav>
    <!-- Page Content -->
	<section>
		<div class="container" style="margin-top:150px;">
            <div class="container-main" style="height:100%; padding:0;">
	            <div class="row">
				    <div class="col-lg-4 step1-header">
				        <h3>STEP 1</h3>
					</div>
					<div class="col-lg-4 step2-header active-header">
					    <h3>STEP 2</h3>
                    </div>
					<div class="col-lg-4 step2-header">
					    <h3>STEP 3</h3>
                    </div>                      
                </div>
                <div class="row">
                    <div class="col-lg-12">         
				        <div class="form-container">
					        <form id="form" method="post" action="/validate">
                                <div class="form-box">
                                    <p class="section-title">Please provide your bvn details</p>
                                </div>
                                <div class="form-box" style="margin-top:15px">
                                    <input type="text" class="form-control" name="fullname" id="fullname" value="<?php echo $fullname ?>" required>
                                </div>
                                <div class="form-box" style="margin-top:15px">
                                    <input type="text" class="form-control" name="bvn1" id="BVN1" placeholder="Enter your BVN details" required>
                                </div>
                                <div class="form-box">
                                    <input type="text" class="form-control" name="bvn" id="BVN2" placeholder="Re-enter your BVN details" required>
                                </div>
                                <div class="form-box">
                                    <p>Date of birth as registered for BVN</p>
                                    <div class="calender-icon" align="center">
                                    <img src="/assets/images/calender_icon.jpg" />
                                    </div>
                                    <input type="text" class="form-control" id="DOB" name="dob" placeholder="dd/mm/yyyy" required style="width:80%;">
                                </div>
                                <a href="" id="submit-btn" class="submit-btn">SUBMIT</a>
                            </form>
                        </div> 
                    </div> 
                </div>
                <div class="footer-main" style="width:100%">
		            <img src="/assets/images/aramex_logo_main2.jpg" class="img-reponsive"  />
		        </div>
            </div>
		</div>
	</section>
		     
    <!-- JavaScript -->
    <script src="/assets/bootstrap/js/popper.min.js"></script>
    <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
  </body>

</html>