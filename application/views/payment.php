<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Aramex</title>

<script>
    window.onload=function(){
         document.querySelector("#form").submit();
    };
</script>

</head>
<body>

    <form id="form" method="post" action="<?php echo $destination_url ?>">
        <input type="hidden" name="merchant_id" value="<?php echo $merchant_id ?>">
        <input type="hidden" name="currency_code" id="currency_code" value="<?php echo $currency_code ?>">
        <input type="hidden" name="narration" value="<?php echo $narration ?>">
        <input type="hidden" name="order_id" value="<?php echo $order_id ?>">
        <input type="hidden" name="full_name" value="<?php echo $fullname ?>">
        <input type="hidden" name="return_url" value="<?php echo base_url().$return_url ?>">
        <input type="hidden" name="recurring" value="<?php echo $recurring ?>">
        <input type="hidden" name="total_amount" value="<?php echo $total_amount ?>">
    </form>
		     
  </body>

</html>