<?php
/**
* 
*/
class Validation_Record_model extends CI_Model
{
	public function store_validation_record($data){

		$this->db->insert('validation_record', $data);
		return $this->db->insert_id();
	}

	public function getBvnID($bvn){

		$bvnID = $this->db 	->select('id')
							->limit(1)
							->where('bvn', $bvn)
							->get('validation_record')
							->row_array();

		if(isset($bvnID['id'])) {
			return $bvnID['id'];
		} else {
			return "";
		}
	}

}
?>