DROP SCHEMA IF EXISTS `shopnship`;
CREATE SCHEMA `shopnship`;
USE shopnship;

CREATE TABLE IF NOT EXISTS `shopnship`.`card_fee` (
	`id` int NOT NULL AUTO_INCREMENT, 
	`fee` double(10,2) NOT NULL, 
	`active` tinyint(2) NOT NULL, 
	`create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, 
	PRIMARY KEY(`id`) 
);

CREATE TABLE IF NOT EXISTS `shopnship`.`rate`(
	`id` int NOT NULL AUTO_INCREMENT,
	`rate` double(10,2) NOT NULL,
	`active` tinyint(2) NOT NULL,
	`create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(`id`)
);

CREATE TABLE IF NOT EXISTS `shopnship`.`service_charge` (
	`id` int NOT NULL AUTO_INCREMENT,
	`charges` double(10,2) NOT NULL,
	`active` tinyint(2) NOT NULL,
	`create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(`id`)
);

CREATE TABLE IF NOT EXISTS `shopnship`.`transaction` (
	`id` int NOT NULL AUTO_INCREMENT,
	`transaction_id` varchar(100) NOT NULL,
	`fullname` varchar(250) NOT NULL,
	`email` varchar(250) NOT NULL,
	`order_id` varchar(100)  NOT NULL,
	`dollar` double(10,2) NOT NULL,
	`naira` double(10,2) NOT NULL,
	`rate` int NOT NULL,
	`service_charge` int NOT NULL,
	`card_fee` int NOT NULL,
	`total` double(10,2) NOT NULL,
	`bank` varchar(256),
	`create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`payment_status` tinyint(1) NOT NULL,
	FOREIGN KEY (rate) REFERENCES rate(id),
	FOREIGN KEY (service_charge) REFERENCES service_charge(id),
	FOREIGN KEY (card_fee) REFERENCES card_fee(id),
	PRIMARY KEY(`id`)
);

CREATE TABLE IF NOT EXISTS `shopnship`.`validation_record` (
	`id` int NOT NULL AUTO_INCREMENT,
	`fullname` varchar(255) NOT NULL,
	`bvn` bigint(11) NOT NULL,
	`dob` char(10) NOT NULL,
	`transaction_id` int NOT NULL,
	`create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`validation_status` tinyint(1) NOT NULL,
	FOREIGN KEY (transaction_id) REFERENCES transaction(id),
	PRIMARY KEY(`id`)
);

CREATE TABLE IF NOT EXISTS `shopnship`.`virtual_card` (
	`id` int NOT NULL AUTO_INCREMENT,
	`card_name` varchar(256) NOT NULL,
	`dollar` double(10,2) NOT NULL,
	`validation_record_id` int NOT NULL,
	`start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  	`end_date` date NULL,
	FOREIGN KEY (validation_record_id) REFERENCES validation_record(id),
  	PRIMARY KEY(`id`)
);

CREATE TABLE IF NOT EXISTS `shopnship`.`range` (
	`id` int NOT NULL AUTO_INCREMENT,
	`max` double(10,2) NOT NULL,
	`min` double(10,2) NOT NULL,
	`active` tinyint(1) NOT NULL,
	`create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(`id`)
);