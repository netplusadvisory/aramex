<?php

class Range_model extends CI_Model {

    public function getActiveRange() {
        $range = $this->db ->select('*')
                    ->limit(1)
                    ->where('active', 1)
                    ->get('range')
                    ->row_array();

        if($range == null)
            return null;

        return (object)array(
            'id' => $range['id'],
            'max' => $range['max'],
            'min' => $range['min'],
            'active' => $range['active'],
            'create_date' => $range['create_date']
        );
    }
}

?>