<?php

/**
* 
*/
class Rate_model extends CI_Model
{
	public function getActiveDollarRate() {

		$dollarRate = $this->db ->select('*')
								->limit(1)
								->where('active', 1)
								->get('rate')
								->row_array();

		if($dollarRate == null)
			return null;

		return (object)array(
				'id' => $dollarRate['id'],
				'rate' => $dollarRate['rate'],
				'acitve' => $dollarRate['active'],
				'create_date' => $dollarRate['create_date']
			);

	}

	public function getDollarRateId(){
		$rate = $this->session->userdata('rate');

		$rateID = $this->db ->select('id')
							->limit(1)
							->where('rate', $rate)
							->get('rate')
							->row_array();
		
	
		if(isset($rateID['id'])){
			return $rateID['id'];
		}else{
			return "";
		}

	}
}

?>