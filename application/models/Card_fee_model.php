<?php

/**
* 
*/
class Card_fee_model extends CI_Model
{
	
	public function getActiveCardFee()
	{

		$cardFee = $this->db ->select('*')
							 ->limit(1)
							 ->where('active', 1)
							 ->get('card_fee')
							 ->row_array();

		if($cardFee == null)
			return null;

		return (object)array(
				'id' => $cardFee['id'],
				'fee' => $cardFee['fee'],
				'active' => $cardFee['fee'],
				'create_date' => $cardFee['create_date'] 
		);


	}

	public function getCardFeeId(){
		$fee = $this->session->userdata('cardFee');

		$cardFeeID = $this->db  ->select('id')
								->limit(1)
								->where('fee', $fee)
								->get('card_fee')
								->row_array();

		// If there is an id then return id
		if(isset($cardFeeID['id'])){
			return $cardFeeID['id'];
		}else{
			return "";
		}

	}
}
?>