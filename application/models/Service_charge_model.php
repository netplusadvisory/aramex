<?php
/**
* 
*/
class Service_charge_model extends CI_Model
{
	
	public function getActiveServiceCharge()
	{
		// SELECT charges FROM service_charge WHERE active = 1;
		$serviceCharge = $this->db  ->select('*')
									->limit(1)
									->where('active', 1)
									->get('service_charge')
									->row_array();

		if($serviceCharge == null)
			return null;

		return (object)array(
					'id' => $serviceCharge['id'],
					'charges' => $serviceCharge['charges'],
					'active' => $serviceCharge['active'],
					'create_date' => $serviceCharge['create_date'] 
		);

	}

	public function getServiceChargeId(){
		$charges = $this->session->userdata('serviceCharge');

		$serviceChargeID = $this->db    ->select('id')
										->limit(1)
										->where('charges', $charges)
										->get('service_charge')
										->row_array();

		// If there is a charge it returns the service charge else null
		if(isset($serviceChargeID['id'])){
			return $serviceChargeID['id'];
		}else{
			return "";
		}

		/*if($serviceChargeID != ""){
			$serviceCharge = (object)array(
					'id' => $serviceChargeID['id'],
					'charges' => $serviceChargeID['charges'],
					'active' => $serviceChargeID['active'],
					'create_date' => $serviceChargeID['create_date'] 
			);

			return $serviceCharge;
		}else{
			return "";
		}*/


	}

}
?>