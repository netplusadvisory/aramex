<?php
	class Transaction extends CI_Model {

		public function getTransactionRate($id){
			$this->db->select('transaction.firstname, transaction.lastname, rate.rate');
			$this->db->from('transaction');
			$this->db->join('rate', 'rate.id ='.$id);
			$query = $this->db->get();
			$data = $query->result_array();
			return $data;
		}

		//get transaction service fee
		public function getTransactionServiceCharge($id){
			$this->db->select('transaction.firstname, transaction.lastname, service_charge.charges');
			$this->db->from('transaction');
			$this->db->join('service_charge', 'service_charge.id ='.$id);
			$query = $this->db->get();
			$data = $query->result_array();
			return $data;
		}

		//get transaction card fee
		public function getTransactionCardFee($id){
			$this->db->select('transaction.firstname, transaction.lastname, card_fee.fee');
			$this->db->from('transaction');
			$this->db->join('card_fee', 'card_fee.id ='.$id);
			$query = $this->db->get();
			$data = $query->result_array();
			return $data;
		}

		// get transaction Voucher
		public function getTransactionVoucher($id){
			$this->db->select('transaction.firstname, transaction.lastname, voucher.voucher');
			$this->db->from('transaction');
			$this->db->join('voucher', 'voucher.id ='.$id);
			$query = $this->db->get();
			$data = $query->result_array();
			return $data;
		}

        // get last transactionID stored 
		public function getLastTransactionId(){
		
			$lastTransactionId = $this->db 	->select('transaction_id') 
									->order_by('id', 'desc')
									->limit(1)
									->get('transaction')
									->row_array();

			if (isset($lastTransactionId['transaction_id'])) {

				return $lastTransactionId['transaction_id'];

			} else {
				return "";
			}

		}

		public function getAllTransaction(){

			$transaction_id = $this->session->userdata('transaction_id');

			
			$transaction = $this->db ->select('*')
							 ->limit(1)
							 ->where('transaction_id', $transaction_id)
							 ->get('transaction')
							 ->row_array();

			if($transaction != ""){
				$transaction = (object)array(
					'id' => $transaction['id'],
					'firstname' => $transaction['firstname'],
					'lastname' => $transaction['lastname'],
					'email' => $transaction['email'],
					'order_id' => $transaction['order_id'],
					'dollar' => $transaction['dollar'],
					'naira' => $transaction['naira'],
					'total' => $transaction['total'],
					'date' => $transaction['date']
				);
				return $transaction;
			}else{
				return "";
			}
			

		}

		// get last paymentID stored 
		public function getLastPaymentId() {
			$lastOrderId = $this->db 	->select('order_id') 
									->order_by('id', 'desc')
									->limit(1)
									->get('transaction')
									->row_array();


			if (isset($lastOrderId['order_id'])) {
				return $lastOrderId['order_id'];
			}else{
				return "";
			}
		}

        public function store_transaction_data($data){
        	// $data is an array
			$this->db->insert('transaction', $data);
			return $this->db->insert_id();
        }

        public function get_transaction_info($orderId){
            $user_data = $this->db  ->select() 
                                    ->limit(1)
                                    ->get_where('transaction', array('order_id' => $orderId))
                                    ->row_array();

            if ($user_data['firstname'] != "") {
                return $user_data;
            }else{
                return "";
            }
        }


}
?>