<?php

class Rate extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('rate_model');
	}


	function get_dollar_rate() {

		$active = $this->input->get('active');

		if($active == true) {

			$dollarRate = $this->session->userdata('active_rate');
            if($dollarRate != null) {
                echo json_encode($dollarRate);
                return;
            }

			$dollarRate = $this->rate_model->getActiveDollarRate();

			if($dollarRate != null) {
				echo json_encode($dollarRate);
				return;
			}
		}

		echo json_encode(array());
	
	}
	
}

?>