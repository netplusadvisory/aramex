
<?php

class Site extends CI_Controller{

	public function __construct() {

		parent::__construct();

		$this->load->library('session');

		$this->load->model('Transaction');
		$this->load->model('Rate_model');
		$this->load->model('Service_charge_model');
		$this->load->model('Card_fee_model');
		$this->load->model('Range_model');
		$this->load->model("Validation_record_model");
		$this->load->model("Virtual_card_model");

		$this->load->helper('url');

		set_exception_handler(array($this, 'exception_handler'));

	}

	function index(){
		$this->load->view('home');
	}

	function start() {

		$activeServiceCharge = $this->Service_charge_model->getActiveServiceCharge();
		$activeRate = $this->Rate_model->getActiveDollarRate();
		$activeCardFee = $this->Card_fee_model->getActiveCardFee();
		$activeRange = $this->Range_model->getActiveRange();

		$this->session->set_userdata('active_service_charge', $activeServiceCharge);
		$this->session->set_userdata('active_rate', $activeRate);
		$this->session->set_userdata('active_card_fee', $activeCardFee);
		$this->session->set_userdata('active_range', $activeRange);

		$this->load->view("step1");

	}

	private function calculateAmount(float $dollarValue) {

		$activeServiceCharge = $this->session->userdata("active_service_charge");
		$activeCardFee = $this->session->userdata("active_card_fee");
		$activeRate = $this->session->userdata("active_rate");

		log_message('debug',"dollar value: ".$dollarValue);
		log_message('debug',"charge: ".$activeServiceCharge->charges);
		log_message('debug',"fee: ".$activeCardFee->fee);
		log_message('debug',"rate: ".$activeRate->rate);


		return ($dollarValue * floatval($activeRate->rate)) + floatval($activeCardFee->fee) + floatval($activeServiceCharge->charges);

	}

	function initiate_payment() {

		$fullname = $this->input->post('fullname');
		$email = $this->input->post('email');

		$this->session->set_userdata("fullname",$fullname);
		$this->session->set_userdata("email",$email);

		$dollarValue = $this->input->post('dollar_value');
		$totalAmount = $this->calculateAmount(floatval($dollarValue));

		//create paymentID
		$prefix = "ARA-PAYMT-";

		$merchantId = "TEST57761f53d9ca8";
		$currencyCode = "NGN";
		$narration = "Aramex virtual card";

		$orderId =  $this->transactionlib->generateId($prefix, "");

		$this->session->set_userdata("order_id",$orderId);
		$this->session->set_userdata('total_amount',$totalAmount);
		$this->session->set_userdata('dollar_value',$dollarValue);

		$returnUrl = "payment_result";
		$recurring = "no";

		$data = array(
			"fullname" => $fullname, 
			"total_amount" => $totalAmount,
			"merchant_id" => $merchantId,
			"currency_code" => $currencyCode,
			"narration" => $narration,
			"return_url" => $returnUrl,
			"recurring" => $recurring,
			"order_id" => $orderId,
			"destination_url" => "https://netpluspay.com/testpayment/paysrc/"
		);

		$this->load->view("payment",$data);
	}

	private function validResponse($code_param) {

		if($code_param == '00')
			return true;

		log_message("debug","invalid response ".$code_param);	
		return false;
	}

	private function validOrderId($order_id_param) {

		if($order_id_param == $this->session->userdata("order_id"))
			return true;

		log_message("debug","invalid order id presented ".$order_id_param);	
		return false;

	}

	private function validAmount($amount_param) {

		if($amount_param == $this->session->userdata("total_amount"))
			return true;

		log_message("debug","amount presented ".$amount_param);	
		return false;
	}

	function sendError($title,$content) {

		if(session_id()) {
			$this->session->sess_destroy();
		}

		$this->load->view('payment_error',array(
			"title" => "Transaction Failed",
			"content" => "Unable to process the transaction. Please check that your card details are correct and try again."
			)
		);
	}

	function payment_result() {

		$code = $this->input->post('code');
		$transaction_id = $this->input->post('transaction_id');
		$order_id = $this->input->post('order_id');
		$amount_paid = $this->input->post('amount_paid');
		$bank = $this->input->post('bank');

		$dollarValue = $this->session->userdata("dollar_value");
		$activeRate = $this->session->userdata("active_rate");
		$activeCardFee = $this->session->userdata("active_card_fee");
		$activeServiceCharge = $this->session->userdata("active_service_charge");
		$fullname = $this->session->userdata("fullname");
		$email = $this->session->userdata("email");

		//log_message("debug",json_encode($user));
		if(!$this->validResponse($code) || !isset($fullname)) {

			//tell them there's something wrong with the card
			$this->Transaction->store_transaction_data(array(
				"transaction_id" => $transaction_id,
				"fullname" => $fullname,
				"email" => $email,
				"order_id" => $order_id,
				"dollar" => $dollarValue,
				"naira" => (floatval($dollarValue) * floatval($activeRate->rate)),
				"rate" => $activeRate->id,
				"service_charge" => $activeServiceCharge->id,
				"card_fee" => $activeCardFee->id,
				"total" => $amount_paid,
				"bank" => $bank,
				"payment_status" => 0
			));

			if(session_id()) {
				$this->session->sess_destroy();
			}

			$this->sendError(
				"Transaction Failed",
				"Unable to process the transaction. Please check that your card details are correct and try again."
			);

		} else if($this->validAmount($amount_paid) && $this->validOrderId($order_id)) {
		
			//create new transaction
			log_message("debug","rate is ".json_encode($activeRate));
			$id = $this->Transaction->store_transaction_data(array(
				"transaction_id" => $transaction_id,
				"fullname" => $fullname,
				"email" => $email,
				"order_id" => $order_id,
				"dollar" => $dollarValue,
				"naira" => (floatval($dollarValue) * floatval($activeRate->rate)),
				"rate" => $activeRate->id,
				"service_charge" => $activeServiceCharge->id,
				"card_fee" => $activeCardFee->id,
				"total" => $amount_paid,
				"bank" => $bank,
				"payment_status" => 1
			));

			$this->session->set_userdata("transaction.id",$id);

			$this->load->view("step2",array("fullname" => $fullname));

		} else {

			$this->sendError(
				"Transaction Failed",
				"An unexpected error occured. Unable to complete the process."
			);

		}
	}

	private function checkBVNDetails($bvn, $dob) {

		if($bvn == "11111111111")
			return true;

		return false;

	}

	private function getVirtualCardDetails($fullname) {

	}

	private function sendBVNInvalidError() {

		if(session_id()) {
			$this->session->sess_destroy();
		}
		$this->load->view("bvn_error");

	}

	function validate_bvn() {

		$transaction_id = $this->session->userdata("transaction.id");

		if(!isset($transaction_id)) {
			//send error.
			$this->sendError(
				"Transaction Failed",
				"An unexpected error occured. Unable to complete the process."
			);
			return;
		}

		$fullname = $this->input->post("fullname");
		$bvn = $this->input->post("bvn");
		$dateOfBirth = $this->input->post("dob");

		$bvnIsValid = $this->checkBVNDetails($bvn,$dateOfBirth);

		if(!$bvnIsValid) {

			$this->Validation_record_model->store_validation_record(array(
				"fullname" => $fullname,
				"bvn" => $bvn,
				"dob" => $dateOfBirth,
				"transaction_id" => $transaction_id,
				"validation_status" => 0
			));

			//send bvn invalid error.
			$this->sendBVNInvalidError();
			return;

		}

		//store bvn validation details in database
		$validation_record_id = $this->Validation_record_model->store_validation_record(array(
			"fullname" => $fullname,
			"dob" => $dateOfBirth,
			"transaction_id" => $transaction_id,
			"validation_status" => 1
		));

		$this->session->set_userdata("validation_record_id",$validation_record_id);
		$this->load->view("step3");

	}

	function create_card() {

		$card_name = $this->input->post("card_name");
		$validation_record_id = $this->session->userdata("validation_record_id");
		$dollarValue = $this->session->userdata("dollar_value");
		$end_date = new DateTime();
		$end_date->setTimeZone(new DateTimeZone('Africa/Lagos'));
		$end_date->modify("+4 years");

		if(!isset($validation_record_id) || !isset($dollarValue)) {
			//send error.
			$this->sendError(
				"Transaction Failed",
				"An unexpected error occured. Unable to complete the process."
			);
			return;
		}

		if(session_id()) {
			$this->session->sess_destroy();
		}

		$this->Virtual_card_model->create_virtual_card(array(
			"dollar" => $dollarValue,
			"validation_record_id" => $validation_record_id,
			"card_name" => $card_name,
			"end_date" => $end_date->format("Y-m-d")
		));

		$this->load->view("success", array( "card_name" => $card_name ));

	}

	public function exception_handler($e) {

		if(session_id()) {
			$this->session->sess_destroy();
		}

		log_message("error", "An expected error occured:\n".$e);
		
		//send error.
		$this->sendError(
			"Error",
			"An unexpected error occured. Unable to complete your request."
		);

	}

}

?>
