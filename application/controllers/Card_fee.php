<?php
/**
* 
*/
class Card_fee extends CI_Controller
{

	public function __construct(){
		parent::__construct();
		$this->load->model('Card_fee_model');
	}

	public function get_card_fee() {

		$active = $this->input->get('active');

		if($active == true)	{

			$cardFee = $this->session->userdata('active_card_fee');
			if($cardFee != null) {
				echo json_encode($cardFee);
				return;
			}

			$cardFee = $this->Card_fee_model->getActiveCardFee();

			if($cardFee != null) {
				echo json_encode($cardFee);
				return;
			}

		}

		return json_encode(array());
	}
}

?>