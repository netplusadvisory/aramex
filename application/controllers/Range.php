<?php

class Range extends CI_controller 
{
    public function __construct()
	{
		parent::__construct();
		$this->load->model('range_model');
    }
    
    public function get_range() {
        $active = $this->input->get('active');

        if($active == true) {

            $range = $this->session->userdata('active_range');
            if($range != null) {
                echo json_encode($range);
                return;
            }

            $range = $this->range_model->getActiveRange();
            if($range != null) {
                echo json_encode($range);
                return;
            }
        }

        echo json_encode(array());

    }
}

?>