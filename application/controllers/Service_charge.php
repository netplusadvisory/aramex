<?php

class Service_charge extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->model('Service_charge_model');
	}

	public function get_service_charge() {
		
		$active = $this->input->get('active');
		
		if($active == true) {

			$serviceCharge = $this->session->userdata('active_service_charge');
            if($serviceCharge != null) {
                echo json_encode($serviceCharge);
                return;
            }


			$serviceCharge = $this->Service_charge_model->getActiveServiceCharge();
			if($serviceCharge != null) {
				echo json_encode($serviceCharge);
				return;
			}
		}

		echo json_encode(array());
		
	}
	
}
?>