<?php

/**
* 
*/
class Customer extends CI_Controller
{

	public function __construct(){
		parent::__construct();
		$this->load->model('Customer_model');
	}
	
	public function get_bvn_data() {
		$bvn = $this->input->post('bvn');

		if($bvn != "" && $bvn == 12345678901){
			$data = (object)array(
				'bvn' => $bvn,
				'firstname' => $this->session->userdata('firstname'),
				'lastname' => $this->session->userdata('lastname'),
				'email' => $this->session->userdata('email')
			);

		 	$this->session->set_userdata('bvn', $data->bvn);  
		   
		   if($data != ""){
		   		echo json_encode($data);
		   		$this->Customer_model->store_customer_details($data);
		   		
		   }
		}
	}

	
}
?>