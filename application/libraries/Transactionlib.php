<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Transactionlib {
    var $CI;
    public function __construct($params = array())
    {
        $this->CI =& get_instance();
        
    }
	// generate transactionId or paymentId for each transaction
	public function generateId($prefix, $lastId){
		// check if there is a previous Id. Else this is the first one
		$lastId = trim($lastId);
		if ($lastId != "" || $lastId != 0) {
			// last id after removal of excess data
			$id = substr($lastId, 16);
			
		}
		else{
			$id = 0;
		}

		$id += 1;

		// geneate six digits random number
		$six_digit_number = mt_rand(100000, 999999);
		$tablename_id = $prefix.$six_digit_number.$id;

		//retuns either transaction id or payment id 
		return $tablename_id;	
	}
}