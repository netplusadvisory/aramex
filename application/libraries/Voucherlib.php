<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Voucherlib
{
	var $CI;

	
	public function __construct($params = array())
	{
		$this->CI =& get_instance();
	}

	public function generateId($lastVoucher){
		// check if there is previous voucher
		$lastVoucher = trim($lastVoucher);
		//generate ten digits random numbers
		$first_number = mt_rand(10000000, 99999999);
		$second_number = mt_rand(10000000, 99999999);

		$twelve_digit = $first_number . $second_number;

		if($lastVoucher != "" && $lastVoucher != 0){
			while($twelve_digit == $lastVoucher){
				$first_number = mt_rand(10000000, 99999999);
				$second_number = mt_rand(10000000, 99999999);

				$twelve_digit = $first_number . $second_number;
			}
			
			return $twelve_digit;
		}else{
			return $twelve_digit;
		}

	}


}