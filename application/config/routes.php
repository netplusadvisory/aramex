<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['service_charge']['get'] = 'service_charge/get_service_charge';
$route['rate']['get'] = 'rate/get_dollar_rate';
$route['card_fee']['get'] = 'card_fee/get_card_fee';
$route['range']['get'] = 'range/get_range';

$route['start']['get'] = 'site/start';
$route['payment_result']["post"] = 'site/payment_result';
$route['validate']['post'] = 'site/validate_bvn';
$route['payment']['post'] = "site/initiate_payment";
$route['create_card'] = 'site/create_card';

$route['default_controller'] = 'site/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
