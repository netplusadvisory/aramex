(function() {

    Number.format = function(n) {
        return  Number.toDouble(n).toLocaleString();
    }

    Number.toDouble = function(n) {
        return Number(Math.round(n+'e2')+'e-2');
    }

    $(document).ready(function() {
        var html5Slider = document.getElementById('dollar');
        $.getJSON('/range',{active : true}, function(obj) {

            var options = { 
                animate : true,
                connect : [true,false]
            }
            options.start = obj.max * 0.25;
            options.range = { max : Number.parseFloat(obj.max), min : Number.parseFloat(obj.min) };
            noUiSlider.create(html5Slider,options);
            init();
            
        });

        var _data = { rate : null, service_charge : null, card_fee : null};

        var setTotal = function() {
            var total = (html5Slider.noUiSlider.get() * _data.rate) + Number.parseFloat(_data.card_fee) + Number.parseFloat(_data.service_charge);
            $("#inside_total_amount").html(Number.format(total));
        }

        var notify = (function(cb,data) {
            var count = 0;
            return function() {
                count++;
                if(count == 3 ) {
                    cb();
                }
            }

        })(setTotal,_data);

        //get rate
        var getRate = function(obj) {
            _data.rate = obj.rate;
            var __data = _data;
            var val = html5Slider.noUiSlider.get() * obj.rate;
            $("#inside_dollar_amount").html(html5Slider.noUiSlider.get());
            $("#inside_naira_equivalent").html(val);
            var slider = html5Slider;
            html5Slider.noUiSlider.on("update",function() {
                var val = slider.noUiSlider.get();
                var equivalent = val * __data.rate;
                var total = equivalent + Number.parseFloat(__data.service_charge) + Number.parseFloat(__data.card_fee);
                $("#inside_dollar_amount").html(val);
                $("#inside_naira_equivalent").html(Number.format(equivalent));
                $("#inside_total_amount").html(Number.format(total));
                $('input#dollar_value').val(val);
                
            });
            html5Slider.noUiSlider.on("change",function() {
                var val = slider.noUiSlider.get();
                $("input#dollar_value").val(val);
            })
            notify();
        }

        var getCardFee = function(obj) {
            _data.card_fee = obj.fee
            $("#inside_card_issuance_fee").html(obj.fee);
            notify();
        };

        var getServiceCharge = function(obj) {
            _data.service_charge = obj.charges;
            $("#inside_service_charge").html(obj.charges);
            notify();
        }

        var init = function() {
            $.getJSON("/rate", { active : true }, getRate);
            $.getJSON("/card_fee",{ active : true },getCardFee);
            $.getJSON("/service_charge",{ active : true }, getServiceCharge);
        }

        $("a.submit-btn").on("click",function(event) {
            event.preventDefault();
            console.log('clicked');
            $("#step1Form").submit();
        });
    });

})()
