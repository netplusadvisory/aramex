// Ajax post
	$(document).ready(function() {

		// error section
		$("p.error_msg").html("");
		// $('.convertToNaira').hide();
		$('.convertToNaira span').html('');
		
		var email = $('input#email').val();

		// Get the dollar conversion rate
		$.getJSON("<?php echo base_url(); ?>" + "index.php/site/get_dollar_rate").done(function(data) {
			// check if ajax works
			alert('the get request works');

			var rate = data.rate;
			// $("input#voucherAmount").val(rate);
			
			// show equivalent of dollars in naira
			$("input#voucherAmount").bind("change paste keyup", function(event) {
				event.preventDefault();
				var voucherAmount = $("input#voucherAmount").val();
				voucherAmount = voucherAmount.trim();
				if (voucherAmount != "") {
					convertToNaira(voucherAmount, rate);
				}else{
					$('.convertToNaira').hide();
				}
				
			});		        
									
		})
		// if rate is not returned or ajax didnt work
		.fail(function(jqXHR, textStatus, errorThrown) {
			$("p.error_msg").html("Rate was not returned <br> Please contact the administrator ");
		});

		
		// validate BVN
		$('input#bvn').bind("paste keyup", function(event) {
			event.preventDefault();
			var bvn = $("input#bvn").val();
			if (bvn != "") {
				if(validateBVN()){
					$('.bvn_status').show();
					$('.bvn_status').html('Invalid BVN');
					$('#user_data').hide();
				}else{
					$('.bvn_status').hide();
					// if bvn is wrong do nothing else show other user information
					// get user data from db
					$.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>" + "index.php/site/get_bvn_data",
						dataType: 'json',
						data: { bvn: bvn },
						success: function(res) {
							if (res){
								// show user bvn data in form
								// alert("firstname: " + res.firstname + " lastname: " + res.lastname + " email: " + res.email);
								$('#firstname').val(res.firstname);
								$('#lastname').val(res.lastname);
								$('#email').val(res.email);
								// show form
								$('#user_data').show();
							}						
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
						    alert("validation from db didnt work");
						}
					});
					
					
				}
			}else{
				$('.bvn_status').hide();
			}
		});

		// validate email
		$('input#email').bind("paste keyup change", function(event) {
			event.preventDefault();
			var email = $("input#email").val();
			if (email != "") {
				if(!validateEmail(email)){
					$('.email_status').show();
					$('.email_status').html('Invalid email');
				}else{
					$('.email_status').hide();
				}
			}else{
				$('.email_status').hide();
			}
		});

		$('#new_email').bind("paste keyup change", function(event) {
			event.preventDefault();
			var email = $('#new_email').val();
			if (email != "") {
				if(!validateEmail(email)){
					$('.email_status').show();
					$('.email_status').html('Invalid email');
				}else{
					$('.email_status').hide();
				}
			}else{
				$('.email_status').hide();
			}
		});


		// on step 1 button click submit form
		$("#step1_button").click(function(event) {
			event.preventDefault();
			var firstname = $("input#firstname").val();
			var lastname = $("input#lastname").val();
			var voucherAmount = $('.convertToNaira span').html();;
			var email = $("input#email").val();
			var bvn = $("input#bvn").val();
			var dollarRate = $("input#dollarRate").val();

			// check if input is valid else gives error...check for this...|| checkIfInputHasMoreThanOneDot(voucherAmount) 
			if (checkIfEmpty(firstname) || checkIfEmpty(lastname) || checkIfEmpty(voucherAmount) || checkIfEmpty(email) || checkIfEmpty(bvn) || !validateEmail(email) ) {
				$("p.error_msg").html("Entry is invalid, Please enter valid details");
			}
			else{
				$("p.error_msg").html("");

				$.ajax({
					type: "POST",
					url: "<?php echo base_url(); ?>" + "index.php/site/user_data_submit",
					dataType: 'json',
					data: {firstname: firstname, lastname: lastname, voucherAmount: voucherAmount, email: email, bvn: bvn, dollarRate: dollarRate},
					success: function(res) {
						if (res){

							// proceed to step 2
							$("#step1").hide();
							$("#step2").show();

							// get session data
							$.getJSON("<?php echo base_url(); ?>" + "index.php/site/get_user_data").done(function(data) {
								// check if ajax works
								alert('the get request works');
								// get user data
								var payment_id = data.payment_id;
						        var amount = data.amount;
						        var email = data.email;

						        $("input[name='order_id']").val(payment_id);
						        $("input[name='total_amount']").val(amount);
						        
									
							})
							// if session is not set or ajax didnt work
							.fail(function(jqXHR, textStatus, errorThrown) {
								$("p.error_msg").html("Nothing was returned <br> An important step has been skipped, Please restart and enter valid details");
								// click to restart hide payment button and display restart button
								// $('#restart').parent().show();
								// $('#pay_').hide();
							});
							// $("#order_id").val();

							// jQuery("div#step3").hide();
							$("#step1_breadcrumbs").removeClass('active');
							$("#step2_breadcrumbs").addClass('active');
							$("#step3_breadcrumbs").removeClass('active');

						}						
						// post worked and session was created
							// alert('values where posted');
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
					    alert("form did not submit error");
					}
				});
			}
		});


		// on step 2 if payment is successful click submit form

		

		// if payment failed show restart button
		// $('#step2_restart').click(function(event) {
		// 	window.location.href='<?php echo base_url(); ?>';
		// });

		

		// on step 3 button click submit form
		$("#step3_button").click(function(event) {
			event.preventDefault();
			// get data if it exists, i.e if session is still intact
			
			var firstname = $("input#new_firstname").val();
			var lastname = $("input#new_lastname").val();
			var email = $("input#new_email").val();

			// get payment_id

			// get session data
			$.getJSON("<?php echo base_url(); ?>" + "index.php/site/get_user_data").done(function(data) {
				// check if ajax works
				alert('the get request works');
				// get user data
				var payment_id = data.payment_id;
		        // check if it has values
		        alert('payment_id: '+payment_id);
				$("p.error_msg").html("");	


				// var bvn = $("input#bvn").val();
				alert('begin');

				// check if input is valid else gives error...check for this...|| checkIfInputHasMoreThanOneDot(voucherAmount) 
				if (checkIfEmpty(email) || !validateEmail(email) ) {
					$("p.error_msg").html("Entry is invalid, Please enter valid details");
				}
				else{
					$("p.error_msg").html("");
					// if user updates their details
					// update the email in the database
					if ($("input#new_email").change()) {
						jQuery.ajax({
							type: "POST",
							url: "<?php echo base_url(); ?>" + "index.php/site/update_user_details",
							dataType: 'json',
							data: {email: email},
							success: function(res) {
								if (res){
									alert('data updated');
								}
							}
						});
					}

					alert('here is working');

					// set values and close session
					jQuery.ajax({
						type: "POST",
						url: "<?php echo base_url(); ?>" + "index.php/site/close",
						dataType: 'json',
						data: {payment_id: payment_id},
						success: function(res) {
							if (res){
								// set values
								alert('post is working');
								// $("#fullname").html(firstname+' '+lastname);
								// go to final step
								jQuery("#step3_data").hide();
								jQuery("#card").show();							
							}
						}
					});					
					
				} //end else

			})
			// if session is not set or ajax didnt work
			.fail(function(jqXHR, textStatus, errorThrown) {
				$("p.error_msg").html("Nothing was returned <br> An important step has been skipped, Please restart and enter valid details");
				// click to restart hide payment button and display restart button
				// $('#restart').parent().show();
				// $('#pay_').hide();
			});
		});

		// close transaction
		// on use card button click submit form
		$("#close").click(function(event) {
			window.location.href='<?php echo base_url(); ?>';
		});

		function getUserData(){
			$.getJSON("<?php echo base_url(); ?>" + "index.php/site/get_user_data").done(function(data) {
				// check if ajax works
				alert('the get request works');
				// get user data
				return data;
		        // check if it has values
		        // alert('payment_id: '+payment_id);
					
			})
		}


		function validateBVN(){
			var bvn = $("input#bvn").val();
			if((bvn.toString().length) != 10){
				// error
				return true;
			}else{
				return false;
			}
		}

		// function validateAccountNumber(){
		// 	var acc = $("input#accountNumber").val();
		// 	if((acc.toString().length) != 10){
		// 		// error
		// 		return true;
		// 	}else{
		// 		return false;
		// 	}
		// }

		// Function that validates email address through a regular expression.
		function validateEmail(Email_input) {
			var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
			if (filter.test(Email_input)) {
				return true;
			}
			else {
				return false;
			}
		}
		
		// function that checks if its empty
		function checkIfEmpty(value){
			// var id = '#'+id;
			// id = '"'+'"';
			// var value = $(id).val();
			if ($.trim(value).length == 0 || value == "") {
				// alert('All fields are mandatory');
				return true;
			}
			else {
				return false;
			}
		}

		// convert to naira
		function convertToNaira(number, rate){
			var nara_value = number * rate;
			// return $nara_value;
			$('.convertToNaira').show();
			$('.convertToNaira span').html(nara_value);
		}


		// function that checks valid number input
		

		// function checkIfInputHasMoreThanOneDot(){

		// }

	});

// Select your input element.
	var voucherAmount = document.getElementById('voucherAmount');
	var bvn = document.getElementById('bvn');
	// var accountNumber = document.getElementById('accountNumber');

	// Listen for input event on numInput. Allows decimal point 110 & 190
	voucherAmount.onkeydown = function(e) {
	    if(!((e.keyCode > 95 && e.keyCode < 106)
	      || (e.keyCode > 47 && e.keyCode < 58) 
	      || e.keyCode == 190
	      || e.keyCode == 110
	      || e.keyCode == 9
	      || e.keyCode == 46
	      || e.keyCode == 8)) {
	        return false;
	    }
	}

	
	bvn.onkeydown = function(e) {
	    if(!((e.keyCode > 95 && e.keyCode < 106)
	      || (e.keyCode > 47 && e.keyCode < 58)
	      || e.keyCode == 9
	      || e.keyCode == 46
	      || e.keyCode == 8)) {
	        return false;
	    }
	}

	// accountNumber.onkeydown = function(e) {
	//     if(!((e.keyCode > 95 && e.keyCode < 106)
	//       || (e.keyCode > 47 && e.keyCode < 58)
	//       || e.keyCode == 9
	//       || e.keyCode == 46
	//       || e.keyCode == 8)) {
	//         return false;
	//     }
	// }
	// ensures you have only one decimal point
	// $('#sub').Click(function(event){
	// 	var number = $('input#number').val();
	function checkIfInputHasMoreThanOneDot(number){
		if ((number.match(/./gi)) == "") {
			return true;
		}
		else if((number.match(/./gi).length) == 1){
			return true;
		}
		else{
			return false;
		}
	}
	// });