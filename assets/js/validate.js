$(document).ready(function(){
	//Tells the user to input the correct firstname
	$("#firstname").focus(function(){
		$('#fname').css("display", "inline");
	});
	$("#firstname").focusout(function(){
		$('#fname').css("display", "none");
	});

	
	//Lastname
	$('#lastname').focus(function(){
		$('#lname').css("display", "inline");
	});
	$('#lastname').focusout(function(){
		$('#lname').css("display", "none");
	})

	//Email
	$('#email').focus(function(){
		$('#mail').css("display", "inline");
	});
	$('#email').focusout(function(){
		$('#mail').css("display", "none");
	})

});

