USE shopnship;

INSERT INTO `card_fee` (`id`, `fee`, `active`, `create_date`) VALUES
(1, 300.00, 1, '2018-01-19 11:19:20');

INSERT INTO `rate` (`id`, `rate`, `active`, `create_date`) VALUES
(1, 320.00, 1, '2018-01-19 11:19:33');

INSERT INTO `service_charge` (`id`, `charges`, `active`, `create_date`) VALUES
(1, 500.00, 1, '2018-01-19 11:19:45');

INSERT INTO `range` (`id`, `max`, `min`, `active`, `create_date`) VALUES
(1, 250.00, 20.00, 1, '2018-01-19 11:19:45');
